<?php

use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use Qiniu\Storage\BucketManager;

define('QINIU_CACHE_PREFIX', 'qiniu:uri:');
define('QINIU_MEDIA_STYLE_PREFIX', 'qiniu:style:uri');
define('QINIU_CACHE_BIN', 'cache');

/**
 * Class QiniuStorageStreamWrapper
 * @description Drupal集成七牛云存储 - StreamWrapper
 */
class QiniuStorageStreamWrapper implements DrupalStreamWrapperInterface {

  private $sk;
  private $ak;
  private $bucket;
  private $token;
  /**
   * @var UploadManager
   */
  private $handler;
  /**
   * @var BucketManager
   */
  private $bucket_manager;
  /**
   * @var Auth
   */
  private $auth;
  private $uri;
  private $base_url;

  private $stream_buffer;
  private $position;
  private $cached;

  private static $mimeTypeMapping;

  public function __construct() {
    // 设置七牛配置
    $this->sk = variable_get('qiniu_sk');
    $this->ak = variable_get('qiniu_ak');

    if (!$this->sk || !$this->ak) throw new Exception('七牛云存储sk 或 ak 未配置', 500);
    $this->bucket = variable_get('qiniu_bucket');
    $this->base_url = variable_get('qiniu_base_url');
    // 初始化七牛组件
    $this->auth = new Auth($this->ak, $this->sk);
    $this->token = $this->auth->uploadToken($this->bucket);
    $this->handler = new UploadManager();
    $this->bucket_manager = new BucketManager($this->auth);
  }

  public function setUri($uri) {
    $this->uri = $uri;
  }

  public function getUri() {
    return $this->uri;
  }

  public function isDrupalStyleUri($uri = null) {
    if (!$uri)
      $uri = $this->uri;

    $mime = self::getMimeType($uri);
    $image_mimes = array(
      'image/png', 'image/jpg', 'image/jpeg', 'image/gif', 'image/bmp'
    );
    $is_drupal_styles_image = strpos($uri, '/styles/') !== FALSE;
    $is_image_media = array_search($mime, $image_mimes) !== FALSE;

    return $is_drupal_styles_image && $is_image_media;
  }

  public function getExternalUrl($uri = null) {
    if (!$uri)
      $uri = $this->uri;

    // Drupal 缩略图额外处理
    if ($this->isDrupalStyleUri($uri)) {
      // 走本地缓存 -> Drupal 自带图片处理
      $this->makeStyleImageAndUploadToQiniu($uri);
      return $this->getDrupalStyleExternalUrl($uri);
    }

    $target = $this->getTarget($uri);
    $external_base_url = variable_get('qiniu_base_url');
    $parts = explode('/' ,$target);
    $filename = array_pop($parts);
    return $external_base_url . '/' . implode('/', $parts). '/'.urlencode($filename);
  }

  public static function getMimeType($uri, $mapping = NULL) {
    if (!isset(self::$mimeTypeMapping)) {
      include_once DRUPAL_ROOT . '/includes/file.mimetypes.inc';
      self::$mimeTypeMapping = file_mimetype_mapping();
    }

    if ($mapping == NULL) {
      $mapping = self::$mimeTypeMapping;
    }

    $extension = '';
    $file_parts = explode('.', basename($uri));

    array_shift($file_parts);

    while ($additional_part = array_pop($file_parts)) {
      $extension = strtolower($additional_part . ($extension ? '.' . $extension : ''));
      if (isset($mapping['extensions'][$extension])) {
        return $mapping['mimetypes'][$mapping['extensions'][$extension]];
      }
    }

    return 'application/octet-stream';
  }

  public function chmod($mode) {
    return TRUE;
  }

  public function realpath() {
    return TRUE;
  }

  public function dirname($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    $scheme = file_uri_scheme($uri);
    $dirname = dirname(file_uri_target($uri));

    if ($dirname == '.') {
      $dirname = '';
    }

    return "$scheme://$dirname";
  }

  public function getTarget($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    list($scheme, $target) = explode('://', $uri, 2);
    return trim($target, '\/');
  }

  public function getSchema($uri = NULL) {
    if (!isset($uri))
      $uri = $this->uri;

    list($scheme, $target) = explode('://', $uri, 2);
    return $scheme;
  }

  public static function convertStyleFileNameToQiniuURI($style_file_name) {
    return str_replace('/', '-', $style_file_name);
  }

  /**
   * 将类似 style/<style_name>/qiniu/filename.png URI 转化为七牛 URI
   * 如 style-style-name-qiniu-filename.png
   * @param null $uri
   * @return string
   */
  public function getDrupalStyleExternalUrl($uri = null) {
    if (!$uri)
      $uri = $this->uri;
    $target = $this->getTarget($uri);
    $external_target = self::convertStyleFileNameToQiniuURI($target);
    return $this->base_url . '/' . $external_target;
  }

  /**
   * 将图片文件本地处理后 再上传到七牛
   * @param null $uri
   */
  public function makeStyleImageAndUploadToQiniu($uri = null) {
    if (!$uri)
      $uri = $this->uri;

    $target = $this->getTarget($uri);
    $parts = explode('/', $target);
    array_shift($parts);
    $style_name = array_shift($parts);
    array_shift($parts);
    $source_target = array_shift($parts);

    $local_uri = 'public://' . $source_target;
    $style_file_local_uri = 'public://' . $target;

    $file_data = file_get_contents($local_uri);
    // 保存本地缓存
    if (!$file_data) {
      $file_data = file_get_contents($this->getDrupalStyleExternalUrl($this->getSchema($uri) . '://' . $source_target));
      file_unmanaged_save_data($file_data, $local_uri);
    }

    $style = image_style_load($style_name);
    //生成本地缩略图
    if (!file_exists($style_file_local_uri)) {
      image_style_create_derivative($style, $local_uri, $style_file_local_uri);
    }

    // 发送到七牛
    $qiniu_stream = fopen($this->getSchema($uri) . '://' . $this->convertStyleFileNameToQiniuURI($target), 'r+');
    $is_exist = fstat($qiniu_stream);
    if (!$is_exist || $is_exist['size'] == 0) {
      $style_file_data = file_get_contents($style_file_local_uri);
      fwrite($qiniu_stream, $style_file_data);
    }
    fclose($qiniu_stream);
  }

  public function stream_open($uri, $mode, $options, &$opened_url) {
    $this->setUri($uri);

    $this->cached = $this->readCache($uri);
    $this->stream_buffer = fopen('php://temp', 'r+');

    // 如果是读文件 则直接把文件从七牛下载到本地
    if (strpos($mode, 'r') !== FALSE && $this->isDrupalStyleUri($uri)) {
      $content = @file_get_contents($this->getExternalUrl($uri));
      if ($content) {
        fwrite($this->stream_buffer, $content);
        fseek($this->stream_buffer, 0);
      }
    }

    return TRUE;
  }

  /**
   * 从缓存读取文件信息
   * @param $uri
   * @return bool
   */
  public function readCache($uri) {
    if (strpos($uri, 'public:///') === 0) {
      $uri = preg_replace('^public://[/]+^', 'public://', $uri);
    } else if (strpos($uri, 'private:///') === 0) {
      $uri = preg_replace('^private://[/]+^', 'private://', $uri);
    }

    $cid = QINIU_CACHE_PREFIX . $uri;
    if ($cached = cache_get($cid, QINIU_CACHE_BIN)) {
      $record = $cached->data;
    } else {
      if (!lock_acquire($cid, 1)) {
        lock_wait($cid);
        $record = $this->readCache($uri);
      } else {
        $record = db_select('qiniu_file', 's')
          ->fields('s')
          ->condition('uri', $uri, '=')
          ->execute()
          ->fetchAssoc();

        cache_set($cid, $record, QINIU_CACHE_BIN, CACHE_TEMPORARY);
        lock_release($cid);
      }
    }

    return $record ? $record : FALSE;
  }

  public function writeUriToCache($uri, $stat = null, $image_info = null) {
    if (strpos($uri, 'public:///') === 0) {
      $uri = preg_replace('^public://[/]+^', 'public://', $uri);
    } else if (strpos($uri, 'private:///') === 0) {
      $uri = preg_replace('^private://[/]+^', 'private://', $uri);
    }
    if (!$stat) {
      $stat = $this->bucket_manager->stat($this->bucket, $this->getTarget($uri));
      $stat = array_shift($stat);
    }
    if (!$image_info) {
      $image_info = get_qiuniu_image_info($uri);
    }

    $data = array(
      'uri' => $uri,
      'filesize' => $stat['fsize'],
      'timestamp' => $stat['putTime'] / 10000000,
      'dir' => 0,
      'hash' => $stat['hash'],
    );
    $data['width'] = $image_info['width'];
    $data['height'] = $image_info['height'];

    db_merge('qiniu_file')
      ->key(array('uri' => $uri))
      ->fields($data)
      ->execute();

    $cid = QINIU_CACHE_PREFIX . $uri;
    cache_clear_all($cid, QINIU_CACHE_BIN);
  }

  public function stream_close() {
    fclose($this->stream_buffer);
    return TRUE;
  }

  public function stream_lock($operation) {
    return fclose($this->stream_buffer);
  }

  public function stream_read($count) {
    return fread($this->stream_buffer, $count);
  }

  public function stream_write($data) {
    $ret = fwrite($this->stream_buffer, $data);
    $this->position += $ret;
    return $this->position;
  }

  public function stream_eof() {
    return feof($this->stream_buffer);
  }

  public function stream_seek($offset, $whence) {
    return fseek($this->stream_buffer, $offset, $whence);
  }

  public function isImage($uri = null) {
    if (!$uri) $uri = $this->uri;
    return strpos($this->getMimeType($uri), 'image') !== FALSE;
  }

  public function stream_flush() {
    $data = stream_get_contents($this->stream_buffer, -1, 0);
    if (empty($data)) {
      return TRUE;
    }
    $file_name = $this->getTarget($this->uri);

    // 先删除
    $this->unlink($this->uri);
    // 再上传
    $results = $this->handler->put($this->token, $file_name, $data, null, $this->getMimeType($this->uri), true);

    if ($this->isImage($this->uri)) {
      list($width, $height) = getimagesizefromstring($data);
      $this->writeUriToCache($this->uri, null, array('width' => $width, 'height' => $height));
    }
    else {
      $this->writeUriToCache($this->uri);
    }

    return TRUE;
  }

  public function stream_tell() {
    return ftell($this->stream_buffer);
  }

  public function stream_stat() {
    $stat = fstat($this->stream_buffer);
    if ($this->cached) {
      $stat['size'] = $stat['fsize'] =$this->cached['filesize'];
    }
    return $stat;
  }

  public function unlink($uri = null) {
    if (!$uri)
      $uri = $this->uri;
    $file_name = $this->getTarget($uri);
    $del_results = $this->bucket_manager->delete($this->bucket, $file_name);
    return TRUE;
  }

  public function rename($from_uri, $to_uri) {
    $this->bucket_manager->move($this->bucket, $this->getTarget($from_uri), $this->bucket, $this->getTarget($to_uri));

    // 增加 to_uri;
    $cache = $this->readCache($from_uri);
    $cache['uri'] = $to_uri;
    drupal_write_record('qiniu_file', $cache);

    // 删除 from_uri
    db_delete('qiniu_file')
      ->condition('uri', $from_uri)
      ->execute();

    return TRUE;
  }

  public function mkdir($uri, $mode, $options) {
    return TRUE;
  }

  public function rmdir($uri, $options) {
    return TRUE;
  }

  public function url_stat($uri, $flags) {
    return TRUE;
  }

  public function dir_opendir($uri, $options) {
    return TRUE;
  }

  public function dir_readdir() {
    return TRUE;
  }

  public function dir_rewinddir() {
    return TRUE;
  }

  public function dir_closedir() {
    return TRUE;
  }
}